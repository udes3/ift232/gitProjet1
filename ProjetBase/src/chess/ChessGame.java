package chess;

import java.awt.Point;
import java.io.File;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

public class ChessGame {
	private ChessBoard board;

	// Charge une planche de jeu a partir d'un fichier.
	public void loadBoard(File file, int boardPosX, int boardPosY) {
		try {
			board = ChessBoard.readFromFile(file, boardPosX, boardPosY);
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR, "Error reading file", ButtonType.OK);
			alert.showAndWait();
			return;
		}
	}

	// Sauvegarde la planche de jeu actuelle dans un fichier.
	public void saveBoard(File file) {

		try {
			board.saveToFile(file);
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR, "Error writing file", ButtonType.OK);
			alert.showAndWait();
			return;
		}
	}
		
	// Démarre l'enregistrement des mouvements du jeu dans un fichier de script.
	public void saveScript(File file) {

		try {
			throw new Exception("Pas implanté!");
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR, "Error writing file", ButtonType.OK);
			alert.showAndWait();
			return;
		}

	}

	// Charge un fichier de script
	public void loadScript(File file) {

		try {
			throw new Exception("Pas implanté!");
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR, "Error reading file", ButtonType.OK);
			alert.showAndWait();
			return;
		}

	}
	public ChessBoard getBoard() {
		return board;
	}
	
	public void movePiece(String string) {
		Point p1=ChessUtils.convertAlgebraicPosition(string.substring(0,2));
		Point p2=ChessUtils.convertAlgebraicPosition(string.substring(3));
		
		board.move(p1, p2);
	}
	
	public boolean compareBoard(ChessBoard result) {
		return board.equals(result);
	}

}
