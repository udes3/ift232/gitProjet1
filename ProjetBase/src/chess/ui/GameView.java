package chess.ui;

import java.io.File;

import chess.ChessBoard;
import chess.ChessGame;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class GameView extends Application {
	// Taille de la fenetre
	private int gameSizeX = 1200;
	private int gameSizeY = 1000;

	// Position de l'echiquier dans la fenetre
	private int boardPosX = 200;
	private int boardPosY = 100;

	// Objet racine de l'interface graphique
	private Scene scene;

	// Dialogue utilisee pour choisir des noms de fichiers
	private FileChooser fileDialog;

	// Panneau principal dans lequel se trouvent les elements de jeu
	private Pane gamePane;

	//
	private ChessGame chessGame=new ChessGame();

	// Methode de demarrage standard pour une application JavaFX.
	// L'initialisation de l'interface graphique se fait ici.
	@Override
	public void start(Stage stage) {

		// Creation des boutons et des actions correspondantes.
		// Les actions sont exprimees sous forme de lambda-expressions (Java 8)

		fileDialog = new FileChooser();
		gamePane = new Pane();


		// Charge la planche de jeu par defaut
		resetGame();

		// Bouton de redemarrage (recharge la planche de jeu par defaut)
		Button resetButton = new Button("Reset");
		resetButton.setLayoutX(250);
		resetButton.setLayoutY(50);
		gamePane.getChildren().add(resetButton);

		resetButton.setOnAction(event -> resetGame());

		// Bouton utilise pour enregistrer les mouvements dnas un script
		Button recordButton = new Button("Record moves");
		recordButton.setLayoutX(325);
		recordButton.setLayoutY(50);
		gamePane.getChildren().add(recordButton);
		recordButton.setOnAction(event -> {

			File file = getSaveFile("Record moves...", "scripts/saves", stage);
			chessGame.saveScript(file);
		});

		// Bouton utilis� pour sauvegarder la planche de jeu
		Button saveButton = new Button("Save board");
		saveButton.setLayoutX(425);
		saveButton.setLayoutY(50);
		gamePane.getChildren().add(saveButton);

		saveButton.setOnAction(event -> {

			File file = getSaveFile("Save Board...", "boards/saves", stage);
			chessGame.saveBoard(file);
		});

		// Bo�te de s�lection utilis�e pour activer l'intelligence artificielle
		CheckBox aiBox = new CheckBox("AI player");
		aiBox.setLayoutX(525);
		aiBox.setLayoutY(50);

		gamePane.getChildren().add(aiBox);

		// Bouton utilise pour charger une planche de jeu
		Button loadButton = new Button("Load board");
		loadButton.setLayoutX(625);
		loadButton.setLayoutY(50);
		gamePane.getChildren().add(loadButton);

		loadButton.setOnAction(event -> {

			File file = getOpenFile("Open Board...", "boards", stage);

			resetGame(file);
		});

		// Bouton utilis� pour charger et executer une ancienne partie
		Button playButton = new Button("Play moves");
		playButton.setLayoutX(725);
		playButton.setLayoutY(50);
		gamePane.getChildren().add(playButton);

		playButton.setOnAction(event -> {
			File file = getOpenFile("Open Script...", "scripts", stage);
			chessGame.loadScript(file);
		});

		// Bouton undo, utilis� pour d�faire le dernier mouvement
		Button undoButton = new Button("Undo");
		undoButton.setLayoutX(825);
		undoButton.setLayoutY(50);
		gamePane.getChildren().add(undoButton);

		// Bouton redo, utilis� pour refaire un mouvement d�fait
		Button redoButton = new Button("Redo");
		redoButton.setLayoutX(900);
		redoButton.setLayoutY(50);
		gamePane.getChildren().add(redoButton);

		// Bo�te de s�lection utilis�e pour activer l'�dition d'�chiquier
		// (d�poser manuellement des pi�ces)
		CheckBox editBox = new CheckBox("Edit board");
		editBox.setLayoutX(200);
		editBox.setLayoutY(950);

		gamePane.getChildren().add(editBox);

		// �tiquette pour la zone de capture des noirs
		Label blackCapture = new Label("Black captures");
		blackCapture.setLayoutX(50);
		blackCapture.setLayoutY(200);
		gamePane.getChildren().add(blackCapture);

		// �tiquette pour la zone de capture des blancs
		Label whiteCapture = new Label("White captures");
		whiteCapture.setLayoutX(1050);
		whiteCapture.setLayoutY(200);
		gamePane.getChildren().add(whiteCapture);

		// Pr�paration de la fen�tre principale
		scene = new Scene(gamePane, gameSizeX, gameSizeY);

		stage.setTitle("Super Mega Chess 3000");

		stage.setScene(scene);

		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

	// Retire la planche de jeu de la fenetre
	private void clearGame() {
		if (chessGame.getBoard()!= null) {
			gamePane.getChildren().remove(chessGame.getBoard().getUI());
		}
	}

	// Redemarre le jeu avec la planche de jeu par defaut.
	private void resetGame() {
		clearGame();

		ChessBoard board = new ChessBoard(boardPosX, boardPosY);
		File file = new File("boards/normalStart");
		chessGame.loadBoard(file, boardPosX, boardPosY);
		gamePane.getChildren().add(chessGame.getBoard().getUI());
		// Attention! Le board peut masquer les autres controles s'il n'est pas
		// place completement derriere eux.
		board.getUI().toBack();
	}

	// Redemarre le jeu avec une planche de jeu chargee d'un fichier
	private void resetGame(File file) {
		clearGame();

		// Obtient la planche de jeu avec ses pieces a partir d'un fichier
		chessGame.loadBoard(file, boardPosX, boardPosY);
		gamePane.getChildren().add(chessGame.getBoard().getUI());
		// Attention! Le board peut masquer les autres controles s'il n'est pas
		// place completement derriere eux.
		chessGame.getBoard().getUI().toBack();
	}

	// Utilise pour obtenir un dialogue d'ouverture
	private File getOpenFile(String title, String baseDir, Stage stage) {

		fileDialog.setTitle(title);
		fileDialog.setInitialDirectory(new File(System.getProperty("user.dir") + "/" + baseDir));
		return fileDialog.showOpenDialog(stage);
	}

	// Utilise pour obtenir un dialogue de sauvegarde
	private File getSaveFile(String title, String baseDir, Stage stage) {

		fileDialog.setTitle(title);
		fileDialog.setInitialDirectory(new File(System.getProperty("user.dir") + "/" + baseDir));
		return fileDialog.showSaveDialog(stage);
	}

}
