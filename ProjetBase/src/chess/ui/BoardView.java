package chess.ui;

import java.awt.Point;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import chess.ChessPiece;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class BoardView {

	// Les cases ont un peu plus de 80 pixels
	public static final double squareSize = 80.3;
	// La bordure fait 80 pixels tout le tour
	public static final int borderSize = 80;
	// L'arrière plan est dans une fenetre 800x800.
	public static final int sceneSize = 800;
	// La bordure fait 80 pixels tout le tour
	public static final int pieceDeltaX = 12;
	// La bordure fait 80 pixels tout le tour
	public static final int pieceDeltaY = 2;
	
	// Panneau d'interface representant la planche de jeu
	private Pane boardPane;
	private ImageView boardView;
	private double startX;
	private double startY;
	
	public BoardView(int x, int y) {
		
		startX = x;
		startY = y;

		// Creation de l'arriere-plan.
		Image boardImage;
		try {
				boardImage = new Image(new FileInputStream("images/board.jpg"));

		} catch (FileNotFoundException e) {

			return;
		}

		boardView = new ImageView(boardImage);

		boardView.setX(x);
		boardView.setY(y);

		boardView.setFitHeight(BoardView.sceneSize);
		boardView.setFitWidth(BoardView.sceneSize);

		boardView.setPreserveRatio(true);

		boardPane = new Pane(boardView);
	}
	
	public Point2D gridToPane(int x, int y) {

		if (x < 0 || x > 7 || y < 0 || y > 7)
			throw new IllegalArgumentException("Piece out of grid: (" + x + "," + y + ")");

		return new Point2D(startX + x * BoardView.squareSize + BoardView.borderSize + BoardView.pieceDeltaX,
				startY + y * BoardView.squareSize + BoardView.borderSize + BoardView.pieceDeltaY);

	}
	
	//Convertit des coordonnees en pixels sur la fenetre d'interface en coordonnees dans la grille de l'echiquier
		//Utilise pour detecter qu'on a touche une case specifique de la grille.
		public Point paneToGrid(double xPos, double yPos) {
			if (xPos < (BoardView.borderSize + startX))
				xPos = BoardView.borderSize + startX;
			if (xPos > startX + BoardView.sceneSize - BoardView.borderSize)
				xPos = startX + BoardView.sceneSize - BoardView.borderSize - (BoardView.squareSize / 2);
			if (yPos < BoardView.borderSize + startY)
				yPos = BoardView.borderSize + startY;
			if (yPos > startY + BoardView.sceneSize - BoardView.borderSize)
				yPos = startY + BoardView.sceneSize - BoardView.borderSize - (BoardView.squareSize / 2);
			int xGridPos = (int) ((xPos - (startX + BoardView.borderSize)) / BoardView.squareSize);
			int yGridPos = (int) ((yPos - (startY + BoardView.borderSize)) / BoardView.squareSize);
			return new Point(xGridPos, yGridPos);
		}
		
	public Pane getPane() {
		return boardPane;
	}

}
