package chess.ui;

import java.awt.Point;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import chess.ChessBoard;
import chess.ChessPiece;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class PieceView {

	// Utilis� pour g�n�rer les noms de fichiers contenant les images des pi�ces.
	private static final String names[] = { "pawn", "knight", "bishop", "rook", "queen", "king" };

	private static final String prefixes[] = { "w", "b" };

	// Taille d'une pi�ce dans l'interface
	private static double pieceSize = 75.0;

	// R�f�rence � la planche de jeu. Utilis�e pour d�placer la pi�ce.
	private ChessBoard board;
	
	private BoardView boardview;

	// Panneau d'interface contenant l'image de la pi�ce
	private Pane piecePane;
	
	//Position initiale d'une piece
	private Point2D posInit;

	public PieceView(ChessBoard b, ChessPiece piece) {

		board = b;

		Image pieceImage;
		try {
			pieceImage = new Image(new FileInputStream("images/" + prefixes[piece.getColor()] + names[piece.getType()] + ".png"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
		ImageView pieceView = new ImageView(pieceImage);

		pieceView.setX(0);
		pieceView.setY(0);
		pieceView.setFitHeight(pieceSize);
		pieceView.setFitWidth(pieceSize);

		pieceView.setPreserveRatio(true);

		piecePane = new Pane(pieceView);
		enableDragging(piecePane, piece);
	}
	
	public PieceView(int x, int y) {
		boardview=new BoardView(x,y);
	}

	public ChessBoard getBoard() {
		return board;
	}

	public void setBoard(ChessBoard board) {
		this.board = board;
	}

	// Gestionnaire d'�v�nements pour le d�placement des pi�ces
	private void enableDragging(Node node, ChessPiece piece) {
		final ObjectProperty<Point2D> mouseAnchor = new SimpleObjectProperty<>();

		// Lorsque la pi�ce est saisie, on pr�serve la position de d�part
		node.setOnMousePressed(event -> {

			mouseAnchor.set(new Point2D(event.getSceneX(), event.getSceneY()));
			posInit=mouseAnchor.get();
		});

		// � chaque �v�nement de d�placement, on d�place la pi�ce et on met �
		// jour la position de d�part
		node.setOnMouseDragged(event -> {
			double deltaX = event.getSceneX() - mouseAnchor.get().getX();
			double deltaY = event.getSceneY() - mouseAnchor.get().getY();
			node.relocate(node.getLayoutX() + deltaX, node.getLayoutY() + deltaY);
			node.toFront();
			mouseAnchor.set(new Point2D(event.getSceneX(), event.getSceneY()));
		});

		// �ce, le mouvement correspondant est appliqu�
		// au jeu d'�checs si posLorsqu'on rel�che la pisible.
		// L'image de la piece est �galement centree sur la case la plus proche.
		node.setOnMouseReleased(event -> {

			Point newGridPos = board.getBoard().paneToGrid(event.getSceneX(), event.getSceneY()); 
			if (board.move(posInit, mouseAnchor.get())) { 
				 
				Point2D newPos = board.getBoard().gridToPane(newGridPos.x, newGridPos.y); 
				node.relocate(newPos.getX(), newPos.getY()); 
				piece.setGridPos(newGridPos); 
			} else { 
				Point2D oldPos = board.getBoard().gridToPane(piece.getGridX(), piece.getGridY()); 
				node.relocate(oldPos.getX(), oldPos.getY()); 
			} 

		});
	}

	public Pane getPane() {
		return piecePane;
	}

	public static String[] getNames() {
		return names;
	}

	public static String[] getPrefixes() {
		return prefixes;
	}
	
}
