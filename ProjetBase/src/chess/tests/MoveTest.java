package chess.tests;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import chess.ChessBoard;
import chess.ChessGame;

class MoveTest {

	// Position de l'echiquier dans la fenetre
	private int boardPosX = 200;
	private int boardPosY = 100;
	ChessGame game = new ChessGame();

	@Test
	public void testBasicCollision() throws Exception {
	File file=new File("boards/normalStart");
	game.loadBoard(file, boardPosX, boardPosY);
	ChessBoard result= ChessBoard.readFromFile("boards/normalStart");
	//Move tower over a pawn of the same color
	game.movePiece("a1-a2");
	assertTrue(game.compareBoard(result));
	}
	
//=================== Test mouvement de chaque piece ============================//
	@Test
	public void testMoveBishop() throws Exception {
		
		File file = new File("boards/testsBishop/bishopBasic");
		game.loadBoard(file, boardPosX, boardPosY);
		ChessBoard result = ChessBoard.readFromFile("boards/testsBishop/apresdeplacement/bishopbasic1");
		game.movePiece("e4-c6");
		assertTrue(game.compareBoard(result));
		
	}
	@Test
	public void testMovePawn()throws Exception{
		File file=new File("boards/testsPawn/pawnBasic");
		game.loadBoard(file, boardPosX, boardPosY);
		ChessBoard result= ChessBoard.readFromFile("boards/testsPawn/apresdeplacement/pawnBasic1");
		//Move pawn one square up
		game.movePiece("e4-e3");
		assertTrue(game.compareBoard(result));
	}
	@Test
	public void testMoveKing()throws Exception{
		File file=new File("boards/testsKing/kingBasic");
		game.loadBoard(file, boardPosX, boardPosY);
		ChessBoard result= ChessBoard.readFromFile("boards/testsKing/apresdeplacement/kingBasic8");
		//Move king up right
		game.movePiece("e4-f5");
		assertTrue(game.compareBoard(result));
	}
	@Test
	public void testMoveRook()throws Exception{
		File file=new File("boards/testsRook/rookBasic");
		game.loadBoard(file, boardPosX, boardPosY);
		ChessBoard result= ChessBoard.readFromFile("boards/testsRook/apresdeplacement/rookBasic2");
		//Move rook all the way up
		game.movePiece("e4-e8");
		assertTrue(game.compareBoard(result));
	}
	@Test
	public void testMoveKnight()throws Exception{
		File file=new File("boards/testsKnight/knightBasic");
		game.loadBoard(file, boardPosX, boardPosY);
		ChessBoard result= ChessBoard.readFromFile("boards/testsKnight/apresdeplacement/knightBasic5");
		//Move knight down left
		game.movePiece("e4-d2");
		assertTrue(game.compareBoard(result));
	}
	@Test
	public void testMoveQueen()throws Exception{
		File file=new File("boards/testsQueen/queenBasic");
		game.loadBoard(file, boardPosX, boardPosY);
		ChessBoard result= ChessBoard.readFromFile("boards/testsQueen/apresdeplacement/queenBasic1");
		//Move queen top left
		game.movePiece("e4-a8");
		assertTrue(game.compareBoard(result));
	}

//================ Test mouvement illegal de chaque piece ========================//
	
	@Test
	public void testIllegalMovePawn()throws Exception{
		File file=new File("boards/testsPawn/pawnBasic");
		game.loadBoard(file, boardPosX, boardPosY);
		ChessBoard result= ChessBoard.readFromFile("boards/testsPawn/pawnBasic");
		//Move pawn right
		game.movePiece("e4-f4");
		assertTrue(game.compareBoard(result));
	}
	@Test
	public void testIllegalMoveKing()throws Exception{
		File file=new File("boards/testsKing/kingBasic");
		game.loadBoard(file, boardPosX, boardPosY);
		ChessBoard result= ChessBoard.readFromFile("boards/testsKing/kingBasic");
		//Move king two square up
		game.movePiece("e4-e6");
		assertTrue(game.compareBoard(result));
	}
	@Test
	public void testIllegalMoveBishop() throws Exception {
		File file = new File("boards/testsBishop/bishopBasic");
		game.loadBoard(file, boardPosX, boardPosY);
		ChessBoard result = ChessBoard.readFromFile("boards/testsBishop/bishopBasic");
		//Move bishop sideways
		game.movePiece("e4-e5");
		assertTrue(game.compareBoard(result));
	}
	@Test
	public void testIllegalMoveQueen()throws Exception{
		File file=new File("boards/testsQueen/queenBasic");
		game.loadBoard(file, boardPosX, boardPosY);
		ChessBoard result= ChessBoard.readFromFile("boards/testsQueen/queenBasic");
		//Move queen as a knight (L)
		game.movePiece("e4-f6");
		assertTrue(game.compareBoard(result));
	}
	@Test
	public void testIllegalMoveKnight()throws Exception{
		File file=new File("boards/testsKnight/knightBasic");
		game.loadBoard(file, boardPosX, boardPosY);
		ChessBoard result= ChessBoard.readFromFile("boards/testsKnight/knightBasic");
		//Move knight one square up
		game.movePiece("e4-e5");
		assertTrue(game.compareBoard(result));
	}
	@Test
	public void testIllegalMoveRook()throws Exception{
		File file=new File("boards/testsRook/rookBasic");
		game.loadBoard(file, boardPosX, boardPosY);
		ChessBoard result= ChessBoard.readFromFile("boards/testsRook/rookBasic");
		//Move rook diagonally
		game.movePiece("e4-f5");
		assertTrue(game.compareBoard(result));
	}
}
