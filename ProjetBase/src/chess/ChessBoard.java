package chess;

import java.awt.Point;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import chess.ui.BoardView;
import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;

//Represente la planche de jeu avec les pieces.

public class ChessBoard {

	// Grille de jeu 8x8 cases. Contient des references aux pieces presentes sur
	// la grille.
	// Lorsqu'une case est vide, elle contient une piece speciale
	// (type=ChessPiece.NONE, color=ChessPiece.COLORLESS).
	private ChessPiece[][] grid;
	private BoardView board;

	public ChessBoard(int x, int y) {

		board = new BoardView(x, y);

		// Initialise la grille avec des pieces vides.
		grid = new ChessPiece[8][8];
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				grid[i][j] = new ChessPiece(i, j, this);
			}
		}

	}

	// Place une piece vide dans la case
	public void clearSquare(int x, int y) {
		grid[x][y] = new ChessPiece(x, y, this);
	}

	// Place une piece vide dans la case
	public void clearSquare(Point pos) {
		clearSquare(pos.x, pos.y);
	}

	public void assignSquare(Point newPos, ChessPiece piece) {
		grid[newPos.x][newPos.y] = piece;
		grid[newPos.x][newPos.y].setGridPos(newPos);
	}

	public ChessPiece getPiece(Point pos) {
		return grid[pos.x][pos.y];
	}

	// Place une piece sur le planche de jeu.
	public void putPiece(ChessPiece piece) {

		Point2D pos = board.gridToPane(piece.getGridX(), piece.getGridY());
		piece.getUI().relocate(pos.getX(), pos.getY());
		getUI().getChildren().add(piece.getUI());
		grid[piece.getGridX()][piece.getGridY()] = piece;
	}

	public void removePiece(Point pos) {
		getUI().getChildren().remove(grid[pos.x][pos.y].getUI());
	}

	public Pane getUI() {
		return board.getPane();
	}

	public BoardView getBoard() {
		return board;
	}

	// Les cases vides contiennent une piece speciale
	public boolean isEmpty(Point pos) {
		return (grid[pos.x][pos.y].getType() == ChessUtils.TYPE_NONE);
	}

	// Verifie si une coordonnee dans la grille est valide
	public boolean isValid(Point pos) {
		return (pos.x >= 0 && pos.x <= 7 && pos.y >= 0 && pos.y <= 7);
	}

	// Verifie si les pieces a deux positions dans la grille sont de la meme
	// couleur.
	public boolean isSameColor(Point pos1, Point pos2) {
		return grid[pos1.x][pos1.y].getColor() == grid[pos2.x][pos2.y].getColor();
	}

	// Effectue un mouvement a partir de la notation algebrique des cases ("e2-b5"
	// par exemple)
	public void algebraicMove(String move) {
		if (move.length() != 5) {
			throw new IllegalArgumentException("Badly formed move");
		}
		String start = move.substring(0, 2);
		String end = move.substring(3, 5);
		move(ChessUtils.convertAlgebraicPosition(start), ChessUtils.convertAlgebraicPosition(end));
	}

	// Effectue un mouvement sur l'echiqier. Quelques regles de base sont implantees
	// ici.
	public boolean move(Point gridPos, Point newGridPos) {
		ChessPiece toMove = getPiece(gridPos);
		// Verifie si les coordonnees sont valides
		if (!isValid(newGridPos))
			return false;

		// Verifie si le mouvement est valide
		if (!toMove.verifyMove(gridPos, newGridPos)) {
			return false;
		}
		
		// Si la case destination est vide, on peut faire le mouvement
		else if (isEmpty(newGridPos)) {
			assignSquare(newGridPos, toMove);
			clearSquare(gridPos);
			return true;
		}

		// Si elle est occuppe par une piece de couleur differente, alors c'est une
		// capture
		else if (!isSameColor(gridPos, newGridPos)) {
			removePiece(newGridPos);
			assignSquare(newGridPos, toMove);
			clearSquare(gridPos);
			return true;
		}

		return false;
	}

	public boolean move(Point2D gridPos, Point2D newGridPos) {
		Point newGridPos1D = board.paneToGrid(newGridPos.getX(), newGridPos.getY());
		Point gridPos1D = board.paneToGrid(gridPos.getX(), gridPos.getY());

		return move(gridPos1D, newGridPos1D);
	}

	// Fonctions de lecture et de sauvegarde d'echiquier dans des fichiers. A
	// implanter.

	public static ChessBoard readFromFile(String fileName) throws Exception {

		return readFromFile(new File(fileName), 0, 0);
	}

	public static ChessBoard readFromFile(File file, int x, int y) throws Exception {
		ChessBoard b = new ChessBoard(x, y);
		ChessPiece chessPiece = new ChessPiece(x, y, b);
		try (Scanner scan = new Scanner(file)) {
			while (scan.hasNextLine()) {
				if (scan.hasNext()) {
					String ligne = scan.next();
					b.putPiece(chessPiece.readFromStream(ligne, b));
				} else {
					break;
				}

			}
		}
		return b;
	}

	public void saveToFile(File file) throws IOException {

		BufferedWriter outputWriter = null;
		outputWriter = new BufferedWriter(new FileWriter(file));
		for (ChessPiece[] Tpiece : grid) {
			for (ChessPiece piece : Tpiece) {
				if (!(piece.isNone())) {
					piece.saveToStream(outputWriter);
					outputWriter.newLine();

				}

			}
		}
		outputWriter.flush();
		outputWriter.close();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ChessBoard) {
			ChessBoard other = (ChessBoard) obj;
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 8; j++) {
					if (!grid[i][j].equals(other.grid[i][j]))
						return false;
				}
			}
			return true;
		}
		return false;
	}

}
