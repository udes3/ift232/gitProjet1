package chess;

import java.awt.Point;
import java.io.BufferedWriter;
import java.io.IOException;

import chess.ui.PieceView;
import javafx.scene.layout.Pane;

public class ChessPiece {

	// Position de la pièce sur l'échiquier
	private int gridPosX;
	private int gridPosY;

	private int type;
	private int color;
	private PieceView piece;

	// Pour créer des pièces à mettre sur les cases vides
	public ChessPiece(int x, int y, ChessBoard chessBoard) {

		this.type = ChessUtils.TYPE_NONE;
		this.color = ChessUtils.COLORLESS;
		gridPosX = x;
		gridPosY = y;
		piece = new PieceView(x, y);
	}

	// Création d'une pièce normale. La position algébrique en notation d'échecs
	// lui donne sa position sur la grille.
	public ChessPiece(String name, String pos, ChessBoard b) {

		color = ChessUtils.getColor(name);
		type = ChessUtils.getType(name);

		piece = new PieceView(b, this);

		setAlgebraicPos(pos);
	}

	// Change la position avec la notation algébrique
	public void setAlgebraicPos(String pos) {

		Point pos2d = ChessUtils.convertAlgebraicPosition(pos);

		gridPosX = pos2d.x;
		gridPosY = pos2d.y;
	}

	// Pour savoir si c'est une pièce vide (pour les cases vides de l'échiquier).
	public boolean isNone() {

		return type == ChessUtils.TYPE_NONE;
	}

	// Accesseurs divers
	public Pane getUI() {
		return piece.getPane();
	}

	public int getType() {
		return type;
	}

	public int getColor() {
		return color;
	}

	public int getGridX() {
		return gridPosX;
	}

	public int getGridY() {
		return gridPosY;
	}

	public Point getGridPos() {
		return new Point(gridPosX, gridPosY);
	}

	public void setGridPos(Point pos) {
		gridPosX = pos.x;
		gridPosY = pos.y;
	}
	
	public boolean verifyMove(Point startPos, Point endPos) {
		int deltax=endPos.x-startPos.x;
		int deltay=endPos.y-startPos.y;
		switch(type){
		case ChessUtils.TYPE_PAWN:
			return deltax==0 && (color==ChessUtils.WHITE && deltay==-1 ||color==ChessUtils.BLACK && deltay==1);
		case ChessUtils.TYPE_KNIGHT:
			deltax=Math.abs(deltax);
			deltay=Math.abs(deltay);
			return (deltax==2 && deltay==1 || deltax==1 && deltay==2);
		case ChessUtils.TYPE_BISHOP:
			deltax=Math.abs(deltax);
			deltay=Math.abs(deltay);
			return (deltax==deltay);
		case ChessUtils.TYPE_ROOK:
			deltax=Math.abs(deltax);
			deltay=Math.abs(deltay);
			return (deltax==0 && deltay>0 && deltay<=7 
					|| deltay==0 && deltax>0 && deltax<=7);
		case ChessUtils.TYPE_QUEEN:
			deltax=Math.abs(deltax);
			deltay=Math.abs(deltay);
			return (deltax==0 && deltay>0 && deltay<=7 
					|| deltay==0 && deltax>0 && deltax<=7
					|| deltax==deltay);
		case ChessUtils.TYPE_KING:
			deltax=Math.abs(deltax);
			deltay=Math.abs(deltay);
			return (deltax<=1 && deltay<=1);
		default: return false;
		}
	}

	public ChessPiece readFromStream(String ligne, ChessBoard b) {
		String pos = ligne.substring(0, 2);
		String name = ligne.substring(3);
		ChessPiece chessPiece = new ChessPiece(name, pos, b);
		return chessPiece;
	}

	public void saveToStream(BufferedWriter writer) throws IOException {

		writer.write(
				ChessUtils.makeAlgebraicPosition(gridPosX, gridPosY) + "-" + ChessUtils.makePieceName(color, type));
		writer.flush();

	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ChessPiece) {
			ChessPiece other = (ChessPiece) obj;
			if(this.color==other.color && this.type == other.type) {
				return true;
			}
		}
		return false;
	}
	
}
